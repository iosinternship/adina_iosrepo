//Chapter2.2 -> Operators in Swift
//Mini exercises


import UIKit

let testNumber = 10
let evenOdd = testNumber % 2


/* %2 verify if testNumber is even or odd
 if the result/evenOdd is 0 then testNumber is even
 if the result/evenOdd is 1 then testNumber is odd
*/

var answer = 0
answer += 1
answer += 10
answer *= 10
answer /= 5

//The answer is 22




