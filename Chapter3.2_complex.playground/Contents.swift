//Chapter 3.2 -> Dictionaries
//Complex

import UIKit


//

var firstLetter = 97
var code: [String : String] = [:]
let endLetter = 122
let message = "hello message"
var codeMessage = ""

while firstLetter <= endLetter{
    if firstLetter == endLetter {
        code[String(UnicodeScalar(UInt8(firstLetter)))] = String(UnicodeScalar(UInt8(97)))
} else {
     let key = String(UnicodeScalar(UInt8(firstLetter)))
    let value = String(UnicodeScalar(UInt8(firstLetter + 1)))
    
    code[key] = value
    }
   firstLetter += 1
}

print(code)

for letter in message {
    let key = "\(letter)"
    if let codeKey = code[key] {
        codeMessage += codeKey
    } else {
        codeMessage += " "
    }
    
}

print(codeMessage)

//

var arrayOfDictionaries = [["firstName" : "Adina", "lastName" : "Sabadis"] , ["firstName" : "Alex", "lastName" : "Vescan"]]

var firstNames: [String] = []
var firstNames2: [String?] = []

for index in arrayOfDictionaries {
    for(key, value) in index {
        if key == "firstName"{
            firstNames.append(value)
        }
        
    }
}
print(firstNames)

//or :)
for index in arrayOfDictionaries {
    firstNames2.append(index["firstName"])
}

print(firstNames2)

//
var fullNames: [String] = []
for index in arrayOfDictionaries {
    var fullName = " "
    for (key, value) in index {
        if key == "firstName" {
            fullName = value + fullName
        } else {
            fullName += value
        }
        
    }
    fullNames.append(fullName)
}
print(fullNames)

var fullNames3: [String] = []
for name in arrayOfDictionaries {
    if let firstName = name["firstName"] {
        if let secondName = name["lastName"] {
            fullNames3.append("\(firstName) \(secondName)")
        }
    }
}

print(fullNames3)
