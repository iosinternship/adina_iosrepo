//Chapter 2.3 -> Work with Number
//Complex exercises

import UIKit

//
var finalsGrade = 2.0
var midTermGrade = 4.0
var projectGrade = 3.0

var totalGrade = (finalsGrade * 50 + midTermGrade * 20 + projectGrade * 30)/100
print(totalGrade)


//
var mealCost = 50.55
var tip: Int = 4

var totalCost = mealCost + Double(tip) * mealCost / 100
print(totalCost)

//
var number = 5.1517
var roundedNumber = Double(10 * number/10)

//
var grade1 = 7.0
var grade2 = 9.0
var grade3 = 5.0
var yourGrade = 8.0

let average = yourGrade > (grade1 + grade2 + grade3 + yourGrade) / 4 ? "above average" : "below average"







