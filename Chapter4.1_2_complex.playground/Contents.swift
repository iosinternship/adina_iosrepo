//Chapter 4.1/ 4.2 -> Functions && Optionals
//Complex

import UIKit

func first(_ N: Int) -> [Int] {
    var arrayNumbers = [Int] ()
    for number in 1...N {
        arrayNumbers.append(number)
    }
    return arrayNumbers
}

first(7)

//

func countdown(_ N: Int) {
    for number in 1...N {
        print("\(number)")
        sleep(1)
    }
    print("Go")
}

countdown(8)

//
func divides(_ a: Int, _ b: Int) -> Bool {
    if a % b == 0 {
        return true
    } else {
        return false
    }
}

//divides(8, 2)

func countDivisors(_ number: Int) -> Int {
    var numberDivisors = 1
    if number == 0 {
        return 1
    }
    for divisor in 1...number/2 {
        if divides(number, divisor) {
            numberDivisors += 1
        }
    }
    return numberDivisors
}

countDivisors(6)

func isPrime(_ number: Int) -> Bool{
    let prime = countDivisors(number)
    if prime == 2 {
        print("The number is prime")
        return true
    } else {
        print("The number isn't prime")
        return false
    }
}

print(isPrime(30))

//
func printFirstPrimes(_ count: Int) -> [Int] {
    var firstPrimes = [Int] ()
    for number in 2...count {
        if isPrime(number) {
            firstPrimes.append(number)
        }
    }
    return firstPrimes
}

print(printFirstPrimes(10))

//
func divideIfWhole(_ firstNumber: Int, _ secondNumber: Int) -> Int? {
    var answer = 1
    if firstNumber == secondNumber {
        print("ep, it divides 1 time")
        return answer
    }
    if firstNumber > secondNumber {
        let remainder = firstNumber % secondNumber
        if remainder == 0 {
            answer = firstNumber / secondNumber
            print("Yep, it divides \(answer) times")
            return answer
        } else {
            print("Not divisible!")
            return nil
        }
    } else {
        let remainder2 = secondNumber % firstNumber
        if remainder2 == 0 {
            answer = secondNumber / firstNumber
            print("Yep, it divides \(answer) times")
            return answer
        } else {
            print("Not divisible!")
            return nil
        }
    }
}

divideIfWhole(3, 9)

//
var queue = [1, 4, 6, 8]

func push(_ number: Int, _ queue: inout [Int]) -> [Int] {
    let length = queue.count
    queue.insert(number, at: length)
    return queue
}

push(3, &queue)

func pop(_ queue: inout [Int]) -> Int? {
    if queue.isEmpty {
        return nil
    } else {
        queue.remove(at: 0)
        return queue[0]
    }
    
}

pop(&queue)

//
var stack = [2, 6, 8, 6, 3]

func pushStack(_ number: Int, _ stack: inout [Int]) -> [Int] {
    stack.insert(number, at: 0)
    return stack
}

pushStack(10, &stack)

func popStack(_ stack: inout [Int]) -> Int? {
    if stack.isEmpty {
        return nil
    } else {
        stack.remove(at: 0)
        return stack[0]
    }
}

print(popStack(&stack) ?? "The stack is empty")

func topStack(_ stack: [Int]) -> Int? {
    if stack.isEmpty{
        return nil
    } else {
        return stack[0]
    }
}

print(popStack(&stack) ?? "The stack is empty")


