//Chapter 3.7/3.8 -> Tuples && Enum
//Complex

import UIKit

var location = (x: 0, y: 0)

enum Direction {
    case up, down, right, left
}

var steps : [Direction] = [.down, .up, .right, .right]

for step in steps {
    switch step {
    case .up:
        location.x += 1
    case .down:
        location.x -= 1
    case .right:
        location.y += 1
    case .left:
        location.y -= 1
    }
}

print(location)

//
var a = (5, 8)
var b = (17, 9)

let (numeratora, denominatora) = a
let (numeratorb, denominatorb) = b

let sum = (numeratora * denominatorb + numeratorb * denominatora, denominatora * denominatorb)

//
enum CoinType: Int {
    case penny = 1
    case nickle = 5
    case dime = 10
    case quarter = 25
}

var moneyArray:[(Int, CoinType)] = [(10, .penny), (15, .nickle), (3, .quarter), (20, .penny), (3, .dime)]
var total = 0

for index in 0...moneyArray.count-1 {
   total += moneyArray[index].0 * moneyArray[index].1.rawValue
    
}

print(total)
