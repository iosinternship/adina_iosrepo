//Chapter 2.4. -> Bolleans
//Complex exercises

import UIKit

let answer = true && true //true
let answer2 = false || false //false
let answer3 = (true && 1 != 2) || (4 > 3 && 100 < 1) //true
let answer4 = ((10 / 2) > 3) && ((10 % 2) == 0) //true

