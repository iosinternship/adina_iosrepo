//Chapter 3.1 -> Array
//Mini exercises

import UIKit

var listOfNumbers = [1, 2, 3, 10, 100]
var max = listOfNumbers[0]

for item in listOfNumbers {
    if item > max {
        max = item
    }
}

print("The max value is \(max)")

//
for item in listOfNumbers {
    if item % 2 != 0 {
        print(item)
    }
}

//
var sum = 0
for item in listOfNumbers {
    sum += item
}

print(sum)

