//Chapter4.3 -> Closures
//Mini
import UIKit

func applyKTimes(_ K: Int, closure: () -> ()) {
    for _ in 1...K {
        closure()
    }
}
