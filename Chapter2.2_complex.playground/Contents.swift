//Chapter 2.2 -> Operators in Swift
//Complex exercises

import UIKit

//Ternary Conditional Operator

let a = 20
let b = -2
let maxNumber = a < b ? b: a

var number: Int?
let c = (number == nil) ? true : false

var boolVariable = true
let d = 25
let e = d + (boolVariable ? 300 : 209)

//Swift Range Operators

let fruits = ["banana", "apple", "cherry", "orange", "grape", "mango"]
let count = fruits.count

for fruit in fruits[..<count] {
    print(fruit)
}

for fruit in fruits[Int(count/2)...] {
    print(fruit)
}


for i in -4..<10 {
    print(i)
}

//Comparison Operators

let firstNumber = 30
let secondNumber = 50
var max: Int
var min: Int

if firstNumber < secondNumber {
     max = secondNumber
     min = firstNumber
    
} else if firstNumber > secondNumber {
    max = firstNumber
    min = secondNumber
} else {
    print("Numbers are equals")
}

let hello = "heyy"
let name = "adina"

if hello == name {
    print(hello + "," + name)
} else {
    print(name + "," + hello)
}

if hello.count <= name.count {
    print(hello.count)
}

//

let a2 = 11
let b2 = 24

let max2 = a2 < b2 ? b2 : a2


//
let number2 = 2
let evenOdd = (number2 % 2 == 0 ? "even" : "odd" )
print(evenOdd)

//

let a3 = 12
let b3 = 3

let isDivisible = (a3 % b3 == 0 ? "divisible" : "not divisible")
print(isDivisible)

//
let a4 = 2
let b4 = 3
let c4 = 2

let sameValue = (a4 == b4 || a4 == c4 || b4 == c4 ? "At least two variables have the same value" : "All the values are different")
print(sameValue)

//
let year = 2014
let leapYear = (((year % 100 == 0 && year % 400 == 0) || (year % 100 != 0 && year % 4 == 0 ) ) ? "Leap year!" : "Not a leap year" )
print(leapYear)










