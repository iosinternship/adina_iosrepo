//Chapter 3.4/3.5 -> For loops && While loops
//Complex

import UIKit

var firstNumber = 1
var endNumber = 5

while firstNumber <= endNumber {
    if firstNumber != endNumber {
        print(firstNumber, endNumber)
    } else {
        print(firstNumber)
    }
    
    firstNumber += 1
    endNumber -= 1
}

var sideSquare = 4
var line = ""
for _ in 1...sideSquare {
    for _ in 1...sideSquare{
        line += "*"
    }
    print(line)
    line = ""
}

//Empty Square

var firstLine = ""
var intermediateLine = ""
for i in 1...sideSquare {
    for j in 1...sideSquare {
        if (i == 1 && j == sideSquare) || (j == 1 && i == sideSquare) {
            for _ in 1...sideSquare {
                firstLine += "*"
            }
            
            print(firstLine)
            
        } else if i != 1 && i != sideSquare {
                if j == 1 || j == sideSquare {
                    intermediateLine += "*"
                    
                } else {
                    intermediateLine += " "
                    
                }
        }
    }
    
    
    if i != 1 && i != sideSquare {
        print(intermediateLine)
    }
    
    firstLine = ""
    intermediateLine = ""
}


//Rectangle

let n = 3
let m = 7
var rectangle = ""

for _ in 1...n {
    for _ in 1...m {
        rectangle += "*"
    }
    print(rectangle)
    rectangle = ""
}

//Reverse number
var number = 1234
var reverseNumber = 0
while number != 0 {
    reverseNumber *= 10
    reverseNumber += number % 10
    number = number/10
}
print(reverseNumber)

//Greatest common divisor
let a = 24
let b = 18
var greatestDivisor = 1
if a < b {
    for divisor in 2...a {
        if a % divisor == 0 && b % divisor == 0 {
            greatestDivisor = divisor
        }
    }
} else if a > b{
    for divisor in 2...b {
        if a % divisor == 0 && b % divisor == 0 {
            greatestDivisor = divisor
        }
    }
} else {
    greatestDivisor = a
}

print(greatestDivisor)

//Prime number
let number2 = 17
var isPrime = false
for divisor in 2...number2/2 {
    if number2 % divisor == 0 {
        print("not prime")
        break
    } else {
        isPrime = true
    }
}

if isPrime {
    print("prime number")
}


