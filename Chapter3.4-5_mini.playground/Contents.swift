//Chapter 3.4/3.5 -> For loops && While loops
//Mini


import UIKit

for _ in 1...10 {
    print("I will not skip the fundamentals!")
}

//

let n = 8
var number = 1
while number <= n {
    print(number * number)
    number += 1
}



