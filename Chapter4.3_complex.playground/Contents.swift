//Chapter 4.3 -> Closures
//Complex

import UIKit

var ints = [1, 2, -3, 4, 5, 6, 8, 9, 3, 12, 11]

var filterClosure = { (_ intParameter: Int) -> Bool
    in
    if intParameter >= 0 {
        return true
    } else {
        return false
    }
}



func filterInts (_ ints: inout [Int], closure: (_ intParameter: Int) -> Bool) {
    var index = 0
   while index  < ints.count {
        if closure(ints[index]) {
            ints.remove(at: index)
            index -= 1
        }
        index += 1
    }
}

filterInts(&ints, closure: filterClosure)


//
var stringValues = ["adina", "alex", "abel"]
var concatedValues = { (_ stringParameter: [String]) -> String
    in
    var concatenatedValue = ""
    let length = stringParameter.count - 1
    for index in 0...length {
        concatenatedValue += stringParameter[index]
    }
    return concatenatedValue
}

func joinStrings (_ strings: [String], closure: ([String]) -> String) {
    let concatenatedString = closure(strings)
}

joinStrings(stringValues, closure: concatedValues)

//
let array1 = [1, 4, 8, 900, 340]
let array2 = [45, 50, 60, 896, 100]
var addInt = { (_ number1: Int, _ number2: Int) -> Int
    in
    return number1 + number2
}

var sumArray = [Int] ()
func combineArrays(_ array1: [Int], _ array2: [Int], closure: (Int, Int) -> Int) {
    for index in 0...array1.count - 1 {
         sumArray.append(closure(array1[index], array2[index]))
    }
}

combineArrays(array1, array2, closure: addInt)
sumArray



