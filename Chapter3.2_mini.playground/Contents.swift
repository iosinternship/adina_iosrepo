//Chapter 3.2 -> Dictionaries
//Mini

import UIKit

var interns = ["Adina" : 24, "Alex" : 23, "Abel" : 23]

for internName in interns.keys {
    print(internName)
}

for internAge in interns.values {
    print(internAge)
}

//
interns.removeValue(forKey: "Abel")
interns.updateValue(60, forKey: "Ion")
print(interns)

