
import UIKit

let value = 10
let multiplier = 5
let sum = "\(value) multiplied by \(multiplier) equals \(value * multiplier)"

//"5 multiplied by 10 equals 50"

let aString = "Replace the e letter with *"
var replacedString = ""
for character in aString {
    if character == "e" {
        replacedString += "*"
    } else {
        replacedString.append(character)
    }
}
print(replacedString)

//

let aString2 = "Hello Swift"
var reverse = ""
let count = aString2.count

for index in aString2 {
    reverse.insert(index, at: reverse.startIndex)
}

print(reverse)

//

var aString3 = "ana"
var reverseString = String(aString3.reversed())
let isPalindrome = (aString3 == reverseString ? true : false)
